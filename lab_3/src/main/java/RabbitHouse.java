/**
Author			: Ardita Sophi Ayustine
Kelas			: DDP 2 - A
NPM				: 1706043701
GitLab account	: arditasophi15
*/



import java.util.Scanner;

public class RabbitHouse{

	public static int permutasi(int banyakAnak){
		// base case
		int i = 1; // i = 1 karena si kelinci induk juga dihitung
		if(banyakAnak == 1){
			return banyakAnak;
			// recursive case
		}else{
			int banyakKelinci = banyakAnak * permutasi(banyakAnak - 1);
			i += banyakKelinci;
			return i;
			}
	}
	
	
	
	// main program
public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Masukkan nama kelinci: ");
		String modeKelinci = input.next();
		String namaKelinci = input.next();
		int banyakAnak = namaKelinci.length();
		
	System.out.println(permutasi(banyakAnak));
	
input.close();
	}
}