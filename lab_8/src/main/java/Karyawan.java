import java.util.ArrayList;

public abstract class Karyawan extends Korporasi{
	protected ArrayList<Karyawan> listBawahan = new ArrayList<Karyawan>();
	protected int counterGaji = 0;
	protected String nama;
	protected int gaji;
	protected String jabatan;
	
	public void setNama(String nama){this.nama = nama;}
	public String getNama(){return nama;}
	public void setGaji(int gaji){this.gaji = gaji;}
	public int getGaji(){return gaji;}
	public void setCounterGaji(int gaji){this.counterGaji = counterGaji;}
	public int getCounterGaji(){return counterGaji;}
	public void setJabatan(String jabatan){this.jabatan = jabatan;}
	public String getJabatan(){return jabatan;}
	public void setListBawahan(ArrayList<Karyawan> listBawahan){this.listBawahan = listBawahan;}
	public ArrayList<Karyawan> getListBawahan(){return listBawahan;}

	public void status(){
		System.out.println(nama + " " + gaji);
	}
	
	public void gajian(){
		counterGaji +=1;
	}
	
	public void naikGaji(){
		//10% dari gaji sebelum
		int gajiAwal = gaji;
		int gajiAkhir = gaji * 11/10; //karena gaji jumlahnya jadi 110%
		gaji += gaji/10;
		System.out.println(nama +" mengalami kenaikan gaji sebesar 10% dari " + gajiAwal + " menjadi "+ gajiAkhir);
	}
	
	public void rekrut(Karyawan bawahan){
		//bisa ngerekrut intern jd bawahan
		if(listBawahan.size() < 10){
			if(canRecruit(bawahan)){
				listBawahan.add(bawahan);
				System.out.println();
			}else{
				System.out.println("ga layak merekrut bawahan");
			}
		}else{
			System.out.println("gabisa nambah lagi boz");
		}
	}
	
	public abstract boolean canRecruit(Karyawan bawahan);
}