public class Intern extends Karyawan{
	
	
	public Intern(String nama, int gaji){
		this.nama = nama;
		this.gaji = gaji;
		this.jabatan = "INTERN";
	}
	public boolean canRecruit(Karyawan bawahan){
		return false;
	}
}