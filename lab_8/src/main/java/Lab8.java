import java.util.Scanner;
import java.util.ArrayList;

class Lab8 {

    public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Korporasi corps = new Korporasi();
		corps.gajiMax = Integer.parseInt(in.nextLine());
		
		while(true){
			String[] perintah = in.nextLine().split(" ");
			if(perintah[0].equals("TAMBAH_KARYAWAN")){
				String nama = perintah [1];
				String jabatan = perintah [2];
				int gaji = Integer.parseInt(perintah[3]);
				corps.addKaryawan(nama, jabatan, gaji);
			}else if(perintah[0].equals("STATUS")){
				String nama = perintah [1];
				corps.status(nama);
			}else if(perintah[0].equals("TAMBAH_BAWAHAN")){
				String namaBos = perintah [1];
				String namaBawahan = perintah [2];
				corps.recruit(namaBos, namaBawahan);
			}else if(perintah[0].equals("GAJIAN")){
				corps.gajian();
			}else if(perintah[0].equals("EXIT")){
				break;
			}else{
				System.out.println("Maaf perintah yang diinginkan tidak tepat.");
			}
		}
		
}
}