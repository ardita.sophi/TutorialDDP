import java.util.ArrayList;

public class Korporasi{
	public ArrayList<Karyawan> listKaryawan = new ArrayList<Karyawan>();
	//punya karyawan <= 10000
	public int gajiMax = 0;
	
	
	public Karyawan findKaryawan(String nama){
		for(int i = 0; i<listKaryawan.size(); i++){
			if(listKaryawan.get(i).getNama().equals(nama)){
				return listKaryawan.get(i);
			}
		}return null;
	}
	
	public Karyawan findRekrutan(String nama, Karyawan karyawan){
		for(int i = 0; i<karyawan.getListBawahan().size(); i++){
			if(karyawan.getListBawahan().get(i).getNama().equals(nama)){
				return karyawan.getListBawahan().get(i);
			}
		}return null;
	}
	
	public void addKaryawan(String nama, String jabatan, int gaji){
		if(listKaryawan.size() < 10000){
			if(findKaryawan(nama) == null){
				if(jabatan.equals("MANAGER")){
					listKaryawan.add(new Manager(nama, gaji));
				}else if(jabatan.equals("STAFF")){
					listKaryawan.add(new Staff(nama, gaji));
				}else if(jabatan.equals("INTERN")){
					listKaryawan.add(new Intern(nama, gaji));
				}
			System.out.println(nama + " mulai bekerja sebagai " + jabatan + " di PT. TAMPAN");
		}else{
			System.out.println("Karyawan dengan nama " + nama + " telah terdaftar");
		}
	}else{
		System.out.println("Korporasi sudah kelebihan karyawan");
	}}
	
	public void removeKaryawan(String nama){
		listKaryawan.remove(listKaryawan.indexOf(findKaryawan(nama)));
	}
	
	public void promosi(Karyawan staff){
		//6 kali naik gaji jadi manager
		String name = staff.getNama();
		int gaji = staff.getGaji();
		ArrayList<Karyawan> listBawahan = staff.getListBawahan();
		int counterGaji = staff.getCounterGaji();
		removeKaryawan(name);
		listKaryawan.add(new Manager(name, gaji));
		findKaryawan(name).setCounterGaji(counterGaji);
		findKaryawan(name).setListBawahan(listBawahan);
		System.out.println("Selamat, " + name + " telah dipromosikan menjadi MANAGER");
	}
	

	public void gajian(){
		System.out.println("Semua karyawan telah diberikan gaji");
		for(int j = 0; j < listKaryawan.size(); j++){
			listKaryawan.get(j).gajian();
			if(listKaryawan.get(j).getCounterGaji() % 6 == 0){
				listKaryawan.get(j).naikGaji();
			}if(listKaryawan.get(j).getJabatan().equals("STAFF")){
				if(listKaryawan.get(j).getGaji() > gajiMax){
					promosi(listKaryawan.get(j));	
				}
			}
		}
	}
	
	public void status(String nama){
		if(findKaryawan(nama) != null){
			findKaryawan(nama).status();
		}else{
			System.out.println("Karyawan tidak ditemukan");
		}
	}
	
	public void recruit(String namaPerekrut, String namaBawahan){
		Karyawan atasan = findKaryawan(namaPerekrut);
		Karyawan bawahan = findKaryawan(namaBawahan);
		if(atasan == null || bawahan == null){
			System.out.println("Nama tidak berhasil ditemukan");
		}else{
			if(atasan.canRecruit() == true){
				for(int p = 0; p < atasan.getListBawahan().size(); p++){
					if(namaBawahan.equals(atasan.getListBawahan().get(p))){
						System.out.println("Karyawan " + namaBawahan + " telah menjadi bawahan " + namaPerekrut);
					}}
			atasan.recruit(bawahan.getNama());
			System.out.println("Karyawan " + namaBawahan + " berhasil ditambahkan menjadi bawahan " + namaPerekrut);
			}else{
				System.out.println("Anda tidak layak memiliki bawahan");
			}
		}
	}

		if(findKaryawan(namaPerekrut) != null && findKaryawan(namaBawahan) != null){
			if(findRekrutan(namaBawahan, findKaryawan(namaPerekrut)) == null){
				findKaryawan(namaPerekrut).recruit(namaPerekrut, namaBawahan);
			}else{
				System.out.println("Karyawan " + namaBawahan + " berhasil ditambahkan menjadi bawahan " + namaPerekrut);
			}
		}else{
			System.out.println("Nama tidak berhasil ditemukan");
		}
}}