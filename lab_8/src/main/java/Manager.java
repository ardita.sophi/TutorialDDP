public class Manager extends Karyawan{
	public Manager(String nama, int gaji){
		this.nama = nama;
		this.gaji = gaji;
		this.jabatan = "MANAGER";
	}
	
	public boolean canRecruit(Karyawan bawahan){
		if(bawahan.getJabatan().equals("STAFF") || bawahan.getJabatan().equals("INTERN")){
			return true;
		}else{
			return false;
		}
	}
}