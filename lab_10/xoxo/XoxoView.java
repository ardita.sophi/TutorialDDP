package xoxo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Ahmad Supriyanto
 */
public class XoxoView extends JFrame {
    
    /**
     * A field that is used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that is used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    /**
     * A field that is used to be the input of Seed.
     */
    private JTextField seedField;

    /**
     * A label to displau "Seed :" before the field.
     */
    private JLabel seedLabel;

    /**
     * A label to display "Message :" before the field.
     */
    private JLabel messageLabel;

    /**
     * A label to display "Key :" before the field.
     */
    private JLabel keyLabel;

    /**
     * A Label to display "Log :" above the text area.
     */
    private JLabel logLabel;

    /**
     * A container that is used to contain a component of gui.
     */
    public Container container;

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        this.container = getContentPane();
        this.container.setLayout(new BorderLayout());
        messageField = new JTextField(10);
        keyField = new JTextField(10);
        seedField = new JTextField("DEFAULT_SEED",10);
        logField = new JTextArea(10,10);
        encryptButton = new JButton("Encrypt");
        decryptButton = new JButton("Decrypt");
        messageLabel = new JLabel("Message          :");
        keyLabel = new JLabel("Key                    :");
        seedLabel = new JLabel("Seed                 :");
        logLabel = new JLabel("Log :");
        setResizable(false);
        setSize(500,300);
        keyField.setSize(300,50);
        messageField.setSize(300,100);
        logField.setSize(500,100);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setTitle("Secret Program");
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }

    /**
     * @return the message field JTextField.
     */
    public JTextField getMessageField() { return messageField; }

    /**
     * @return the key field JTextField.
     */
    public JTextField getKeyField() { return keyField; }

    /**
     * @return the log text area JTextArea.
     */
    public JTextArea getLogField() { return logField; }

    /**
     * @return the Encrypt Button JButton.
     */
    public JButton getEncryptButton() { return encryptButton; }

    /**
     * @return the Encrypt Button JButton.
     */
    public JButton getDecryptButton() { return decryptButton; }

    /**
     * @return
     */
    public JLabel getMessageLabel() { return messageLabel; }

    /**
     * @return key label JLabel.
     */
    public JLabel getKeyLabel() { return keyLabel; }

    /**
     * @return log label JLabel.
     */
    public JLabel getLogLabel() { return logLabel; }

    /**
     * @return Seed field JTextField.
     */
    public JTextField getSeedField() { return seedField; }

    /**
     * @return seed label JLabel.
     */
    public JLabel getSeedLabel() { return seedLabel; }

    /**
     * @return seed text.
     */
    public String getSeedText() { return seedField.getText(); }
}