package xoxo;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.HugKey;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Ahmad Supriyanto
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        gui.container.add(CreateMessagePanel(), BorderLayout.PAGE_START);
        Encrypting();
        Decrypting();
    }

    /**
     * Create a panel that is used as a layer for buttons, fields, and text area.
     * @return panel to display the gui panel.
     */
    public JPanel CreateMessagePanel (){
        JPanel messagePanel = new JPanel();
        messagePanel.setLayout(new GridLayout(3,2));
        messagePanel.add(gui.getMessageLabel());
        messagePanel.add(gui.getMessageField());
        messagePanel.add(gui.getKeyLabel());
        messagePanel.add(gui.getKeyField());
        messagePanel.add(gui.getSeedLabel());
        messagePanel.add(gui.getSeedField());

        JPanel ButtonPanel = new JPanel();
        ButtonPanel.setLayout(new BorderLayout());
        ButtonPanel.add(gui.getEncryptButton(), BorderLayout.PAGE_START);
        ButtonPanel.add(gui.getDecryptButton(), BorderLayout.PAGE_END);

        JPanel InputPanel = new JPanel();
        InputPanel.add(messagePanel);
        InputPanel.add(ButtonPanel);

        JPanel outputPanel = new JPanel();
        outputPanel.setLayout(new BorderLayout());
        outputPanel.add(gui.getLogLabel(), BorderLayout.PAGE_START);
        outputPanel.add(gui.getLogField(), BorderLayout.PAGE_END);


        JPanel MainPanel = new JPanel();
        MainPanel.setLayout(new BorderLayout());
        MainPanel.add(InputPanel, BorderLayout.PAGE_START);
        MainPanel.add(outputPanel, BorderLayout.PAGE_END);

        return MainPanel;
    }

    /**
     * Create a listener that is used when user click the encrypt button.
     */
    public void Encrypting() {
        gui.setEncryptFunction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                XoxoEncryption Encryption = new XoxoEncryption(gui.getKeyText());
                if (gui.getSeedText().equals("")) {
                    try {
                        gui.appendLog(Encryption.encrypt(gui.getMessageText()).getEncryptedMessage());
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    } catch (SizeTooBigException e1) {
                        e1.printStackTrace();
                    }
                } else if (gui.getSeedText().equals("DEFAULT_SEED")) {
                    try {
                        gui.appendLog(Encryption.encrypt(gui.getMessageText(), HugKey.DEFAULT_SEED).getEncryptedMessage());
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    } catch (SizeTooBigException e1) {
                        e1.printStackTrace();
                    }
                }else {
                    try {
                        gui.appendLog(Encryption.encrypt(gui.getMessageText(), Integer.parseInt(gui.getSeedText())).getEncryptedMessage());
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    } catch (SizeTooBigException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
    }

    /**
     * Create a listener that is used when user click the decrypt button.
     */
    public void Decrypting() {
        gui.setDecryptFunction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                XoxoDecryption Decryption = new XoxoDecryption(gui.getKeyText());
                if (gui.getSeedText().equals("DEFAULT_SEED")) {
                    gui.appendLog(Decryption.decrypt(gui.getMessageText(), HugKey.DEFAULT_SEED));
                } else {
                    gui.appendLog(Decryption.decrypt(gui.getMessageText(), Integer.parseInt(gui.getSeedText())));
                }
            }
        });
    }
}