package xoxo.crypto;

import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.key.HugKey;
import xoxo.key.KissKey;
import xoxo.util.XoxoMessage;

import javax.swing.*;
import java.io.UnsupportedEncodingException;

/**
 * This class is used to create an encryption instance
 * that can be used to encrypt a plain text message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Ahmad Supriyanto
 */
public class XoxoEncryption {

    /**
     * A Kiss Key object that is required to encrypt the message.private KissKey kissKey;
     */
    private KissKey kissKey;

    /**
     * Class constructor with the given Kiss Key
     * string to build the Kiss Key object.
     * 
     * @throws KeyTooLongException if the length of the
     *         kissKeyString exceeded 28 characters.
     */
    public XoxoEncryption(String kissKeyString) throws KeyTooLongException{
        this.kissKey = new KissKey(kissKeyString);
    }

    /**
     * Encrypts a message in order to make it unreadable.
     * 
     * @param message The message that wants to be encrypted.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     */
    public XoxoMessage encrypt(String message) throws UnsupportedEncodingException, SizeTooBigException {
        String encryptedMessage = this.encryptMessage(message); 
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey));
    }

    /**
     * Encrypts a message in order to make it unreadable.
     * 
     * @param message The message that wants to be encrypted.
     * @param seed A number to generate different Hug Key.
     * @return A XoxoMessage object that contains the encrypted message
     *         and a Hug Key object that can be used to decrypt the message.
     * @throws RangeExceededException if ... <complete this>
     */
    public XoxoMessage encrypt(String message, int seed) throws UnsupportedEncodingException, SizeTooBigException {

        //TODO: throw RangeExceededException for seed requirements
        CheckSeedSize(seed);

        String encryptedMessage = this.encryptMessage(message);
        return new XoxoMessage(encryptedMessage, new HugKey(this.kissKey, seed));
    }

    /**
     * Runs the encryption algorithm to turn the message string
     * into an ecrypted message string.
     * 
     * @param message The message that wants to be encrypted.
     * @return The encrypted message string.
     * @throws SizeTooBigException if the message size is longer than 1Kbit.
     * @throws InvalidCharacterException if message is not a valid character (Letter and "@").
     */
    private String encryptMessage(String message) throws UnsupportedEncodingException, SizeTooBigException {

        //TODO: throw SizeTooBigException for message requirements
        CheckStringSize(message);

        final int length = message.length();
        String encryptedMessage = "";
        for (int i = 0; i < length; i++) {

            //TODO: throw InvalidCharacterException for message requirements
            try {
                CheckValidationCharacter(message);
            } catch (InvalidCharacterException e) {
                e.printStackTrace();
                break;
            }

            int m = message.charAt(i);
            int k = this.kissKey.keyAt(i) - 'a';
            int value = m ^ k;
            encryptedMessage += (char) value;
        }
        return encryptedMessage;
    }

    /** Check the seed whether it is in the range between 0 and 36.
     * @param seed A number to generate different Hug Key.
     */
    private void CheckSeedSize(int seed) {
        if (seed < HugKey.MIN_RANGE || seed > HugKey.MAX_RANGE) {
            JOptionPane.showMessageDialog(null, "Seed length is invalid");
            throw new RangeExceededException("Seed length is invalid");
        }
    }

    /**
     * Check the message size whether it is less than 1000.
     * @param message
     * @throws UnsupportedEncodingException Exception for unsupportedEncoding when checking the message size.
     * @throws SizeTooBigException Exception for the message size that is more than 1Kbit.
     */
    public void CheckStringSize(String message) throws UnsupportedEncodingException, SizeTooBigException {
        if (message.getBytes("UTF-8").length > 1000) {
            JOptionPane.showMessageDialog(null, "Size of the message is too big !");
            throw new SizeTooBigException("Size of the message is too big !");
        }
    }

    /**
     * Check the character whether it is a letter or "@".
     * @param message The message that wants to be encrypted.
     * @throws InvalidCharacterException Exception for the character that is invalid.
     */
    public void CheckValidationCharacter(String message) throws InvalidCharacterException {
        final String CheckChar = "@";
        for (int i = 0 ; i < message.length() ; i++) {
            if (!Character.isLetter(message.charAt(i))) {
                if (String.valueOf(message.charAt(i)).equals(CheckChar)) {
                    return;
                }
                JOptionPane.showMessageDialog(null, "Not Valid Character !");
                throw new InvalidCharacterException("Not Valid Character : " + message.charAt(i));
            }
        }
    }
}

