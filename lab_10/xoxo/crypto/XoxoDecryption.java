package xoxo.crypto;

import xoxo.key.KissKey;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Ahnad Supriyanto
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) {
        this.hugKeyString = hugKeyString;
    }

    /**
     * Decrypts an encrypted message.
     * 
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */

    public String decrypt(String encryptedMessage, int seed) {
        //TODO: Im
        String decryptedMessage = "";
        for (int i = 0 ; i < encryptedMessage.length() ; i++) {
            int m = encryptedMessage.charAt(i);
            int k = (hugKeyString.charAt(i % hugKeyString.length()) ^ seed) - 'a';
            int value = m ^ k;
            decryptedMessage += (char) value;
        }
        return decryptedMessage;
    }
}