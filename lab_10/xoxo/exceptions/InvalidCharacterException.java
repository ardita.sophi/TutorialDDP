package xoxo.exceptions;

/**
 * This is an Exception when a user input an invalid character.
 * Valid character are Letter and "@".
 */
public class InvalidCharacterException extends RuntimeException {

    //TODO: Implement the exception
    public InvalidCharacterException(String message){
        super(message);
    }
}