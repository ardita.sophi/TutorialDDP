package xoxo.exceptions;

/**
 * An exception that is thrown when the seed length is not between 0 and 36.
 */
public class RangeExceededException extends RuntimeException {

    //TODO: Implement the exception
    public RangeExceededException(String message){
        super(message);
    }
}