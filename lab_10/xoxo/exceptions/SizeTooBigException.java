package xoxo.exceptions;

/**
 * An Exception that is thrown when the message size exceeds 1Kbits.
 */
public class SizeTooBigException extends RuntimeException {

    //TODO: Implement the exception
    public SizeTooBigException(String message) {
        super(message);
    }
}