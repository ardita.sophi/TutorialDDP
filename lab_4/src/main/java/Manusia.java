/**
Author			: Ardita Sophi Ayustine
Kelas			: DDP 2 - A
NPM				: 1706043701
GitLab account	: arditasophi15
*/


public class Manusia {
	private String nama;
	private int umur;
	private int uang = 50000;
	private float kebahagiaan = 50;
	
		public Manusia(String nama, int umur){
			this.nama = nama;
			this.umur = umur;
		}
		
		public Manusia(String nama, int umur, int uang){
			this.nama = nama;
			this.umur = umur;
			this.uang = uang;
		}
		
		public void setNama(String nama){
			this.nama = nama;
		}
		
		public String getNama(){
			return nama;
		}
		
		public void setUmur(int umur){
			this.umur = umur;
		}
		
		public int getUmur(){
			return umur;
		}

		public void setUang(int uang){
		    this.uang = uang;
		}
		
		public int getUang(){
		    return uang;
		}

		
		public void setKebahagiaan(float kebahagiaan){
			if (kebahagiaan < 0){
				kebahagiaan = 0;
			}else if(kebahagiaan >= 100){
					kebahagiaan = 100;
			}
			this.kebahagiaan = kebahagiaan;
			}
				
		public float getKebahagiaan(){
			return kebahagiaan;
		}

		
		public void beriUang(Manusia penerima){
			int huruf = 0;
			for(int i=0;i<penerima.nama.length();i++){
				char chr = penerima.nama.charAt(i);
				int chrInt = (int) chr;
				huruf += chrInt;
			}
			int jumlahUang = huruf * 100;
			if(this.uang < jumlahUang){
				System.out.println(this.nama + " ingin memberi uang kepada " + penerima.nama + " tetapi tidak punya cukup uang :'(");
			}else{
				this.uang = this.uang - jumlahUang;
				penerima.uang += jumlahUang;
				this.setKebahagiaan(this.kebahagiaan + (float) jumlahUang / 6000);
				penerima.setKebahagiaan(penerima.kebahagiaan + (float) jumlahUang / 6000);
				System.out.println(this.nama + " memberi uang sebanyak " + jumlahUang + " kepada " + penerima.nama + ", mereka berdua senang :D");
			}
		}
		
		public void beriUang(Manusia penerima, int jumlah){
			if(this.uang < jumlah){
				System.out.println(this.nama + " ingin memberi uang kepada " + penerima.nama + " tetapi tidak punya cukup uang :'(");
			}else{
				this.uang = this.uang - jumlah;
				penerima.uang += jumlah;
				this.setKebahagiaan(this.kebahagiaan + (float) jumlah / 6000);
				penerima.setKebahagiaan(penerima.kebahagiaan + (float) jumlah / 6000);
				System.out.println(this.nama + " memberi uang sebanyak " + (int) jumlah + " kepada " + penerima.nama + ", mereka berdua senang :D");
			}
		}
		
		public void bekerja(int durasi, int bebanKerja){
			int pendapatan = 0;
			if (umur < 18){
				System.out.println(nama + " belum boleh bekerja karena masih dibawah umur :(");
			}else{
				int BebanKerjaTotal = durasi * bebanKerja;
				if (BebanKerjaTotal <= this.kebahagiaan){
					this.setKebahagiaan(this.kebahagiaan - BebanKerjaTotal);
					pendapatan += (BebanKerjaTotal * 10000);
					System.out.println(nama + " bekerja full time, total pendapatan: " + pendapatan);
				}else{
					int DurasiBaru = (int) (this.kebahagiaan / (float) bebanKerja);
					float BebanKerjaNow = DurasiBaru * (float) bebanKerja;
					pendapatan += BebanKerjaNow * 10000;
					this.setKebahagiaan(this.kebahagiaan - BebanKerjaNow);
					System.out.println(this.nama + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan: " + pendapatan);
				}
				this.uang += pendapatan;
			}
		}
		
		public void sakit(String namaPenyakit){
			this.setKebahagiaan(this.kebahagiaan - namaPenyakit.length());
			System.out.println(this.nama + " terkena penyakit " + namaPenyakit + ":O");
		}
		
		public void rekreasi(String namaTempat){
			int biaya = namaTempat.length() * 10000;
			if(this.uang < biaya){
				System.out.println(this.nama + " tidak mempunyai cukup uang untuk rekreasi di " + namaTempat + ":(");
			}else{
				this.uang -= biaya;
				this.setKebahagiaan(this.kebahagiaan + namaTempat.length());
				System.out.println(this.nama + " berekreasi di " + namaTempat + ", " + this.nama + " senang yey :D");
			}
		}
		public String toString(){
		return ("Nama\t\t: " + this.nama + "\n" + "Umur\t\t: " + this.umur + "\n" + "Uang\t\t: " + this.uang + "\n" + "Kebahagiaan\t: " + this.kebahagiaan);
		}
}