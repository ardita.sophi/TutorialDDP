import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    
    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
		Player player1 = null;
		for(int i = 0; i<player.size(); i++){
			if(name.equals(player.get(i).getName())){
				player1 = player.get(i);
			}
		}return player1;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician, dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
		if(find(chara) != null){
			return "Sudah ada karakter bernama " + chara;
		}else{
			if(tipe.equals("Human")){
				player.add(new Human(chara, hp));
			}else if(tipe.equals("Magician")){
				player.add(new Magician(chara, hp));
			}else if(tipe.equals("Monster")){
				player.add(new Monster(chara, hp));
			}
			return chara + " ditambah ke game";
    }}

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
		if(find(chara) != null){
			return "Sudah ada karakter bernama " + chara;
		}else{
			if(tipe.equals("Human")){
				player.add(new Human (chara, hp));
			}else if(tipe.equals("Magician")){
				player.add(new Magician (chara, hp));
			}else if(tipe.equals("Monster")){
				player.add(new Monster(chara, hp, roar));
			}
			return chara + " ditambah ke game";
    }
	}

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
		if(find(chara) == null){
			return "Tidak ada " + chara;
		}
		else{
			player.remove(find(chara));
			return chara + " dihapus dari game";
		}
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
		if(find(chara) == null){
			return "Tidak ada " + chara;
		}
		else{
			Player pemain = find(chara);
			String output = pemain.getTipe() + " " + pemain.getName() + "\nHp: " + pemain.getHp();
			if(pemain.getIsDead() == true){
				output = output + "\nSudah meninggal dunia dengan damai\n";
			}else{
				output = output + "\nMasih hidup\n";
			}if(pemain.getDiet().size() == 0){
				output = output + "Belum memakan siapa siapa";
			}else{
				output = output + "Memakan " + diet(chara);
			}
			return output;
		}
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
		String out = "";
		for(int i = 0; i<player.size(); i++){
			out = out + status(player.get(i).getName()) + "\n";
		}
        return out;        
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
		if(find(chara) == null){
			return "Tidak ada " + chara;
		}else{
			Player pemain = find(chara);
			if(pemain.getDiet().size() == 0){
				return "";
			}else{
				String out = "";
				for(int i = pemain.getDiet().size()-1; i >= 0; i--){
					if(i != 0 ){
						out = out + pemain.getDiet().get(i) + ", ";
					}else{
						out = out + pemain.getDiet().get(i);
					}
				}return out;
			}
		}
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
		String out = "";
		ArrayList<String> eaten = new ArrayList<>();
		for (int i = 0; i < player.size() ; i++){
			String nama = player.get(i).getName();
			String memakan = diet(nama);
			if(!memakan.equals("")){
				eaten.add(memakan);
			}
		}out = out + "Termakan : ";
		for(int j = 0; j < eaten.size(); j++){
			if(j != 0){
				out = out + eaten.get(j) + ", ";
			}else{
				out = out + eaten.get(j);
			}
		}
        return out;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
		Player me = find(meName);
		Player enemy = find(enemyName);
		me.attack(enemy);
        return "Nyawa "+ enemyName + " " + enemy.getHp();
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
		Player me = find(meName);
		Player enemy = find(enemyName);
		String out = "";
		if(me.getTipe().equals("Magician")){
			((Magician)me).burn(enemy);
			out = out + "Nyawa " + enemyName + " " + enemy.getHp();
			if(enemy.getIsBurn() == true){
				out = out + "\n dan matang";
			}return out;
		}else{
			return meName + " Tidak bisa burn " + enemyName;
		}
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
		Player me = find(meName);
		Player enemy = find(enemyName);
		if(me.canEat(enemy) == true){
			me.setHp(me.getHp() + 15);
			me.memangsa(enemy.getTipe() + " " + enemy.getName());
			player.remove(enemy);
			return meName + " memakan " + enemyName + "\nNyawa " + meName + " kini " + me.getHp();
		}else{
			return meName + " tidak bisa memakan " + enemyName;
		}
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
		if(find(meName) == null){
			return "Tidak ada " + meName;
		}else{
			if(find(meName).getTipe().equals("Human") || find(meName).getTipe().equals("Magician")){
				return meName + " tidak bisa berteriak";
			}else{
				return ((Monster)find(meName)).roar();
			}
		}
	}
}