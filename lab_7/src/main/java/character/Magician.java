package character;

//  write Magician Class here

public class Magician extends Player{
	
	public Magician(String name, int hp){
		super(name, hp);
		this.tipe = "Magician";
		//human yang bisa burn
		//kalo kena burn berkurang 10
		//akan terkena damage 2x lipat ketika dijadikan target untuk method attack dan burn
	}
	public void burn(Player burnt){
		if(burnt.getTipe().equals("Magician")){
			burnt.setHp(burnt.getHp() - 20);
		}else{
			burnt.setHp(burnt.getHp() - 10);
		}
		if(burnt.getHp() <= 0){
			burnt.setIsBurn(true);
		}
	}
}