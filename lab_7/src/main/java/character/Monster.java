package character;

//  write Monster Class here
public class Monster extends Player{
	private String roar;
	
	public Monster(String name, int hp){
		super(name, hp*2);
		this.tipe = "Monster";
		this.roar = "AAAAAAaaaAAAAAaaaAAAAAA";
		//bisa makan semua hal yang sudah mati, bisa makan sesama monster
		//bisa roar. defaultnya: "AAAAAAaaaAAAAAaaaAAAAAA"
		//nyawa 2 kali lipat yang di assign
	}
	public Monster(String name, int hp, String roar){
		super(name, hp*2);
		this.tipe = "Monster";
		this.roar = roar;
	}
	public String roar(){
		return roar;
	}
}