package character;
import java.util.ArrayList;

//  write Player Class here
public class Player {
	protected String name;
	protected int hp;
	protected ArrayList<String> diet;
	protected String tipe;
	protected boolean isDead = false;
	protected boolean isBurn = false;
	
	public Player(String name, int hp){
		this.name = name;
		if(hp<= 0){
			this.isDead = true;
		}
		this.hp= hp;
		this.diet = new ArrayList<String>();
	}
	
	public String getName(){return name;}
	public void setName(String name){this.name = name;}
	
	public int getHp(){return hp;}
	public void setHp(int hp){
		if(hp <= 0){
			this.hp = 0;
			setIsDead(true);
		}else{
			this.hp = hp;
		}
	}
	
	public ArrayList<String> getDiet(){return diet;}
	
	public boolean getIsDead(){return isDead;}
	public void setIsDead(boolean isDead2){this.isDead = isDead2;}
	
	public boolean getIsBurn(){return isBurn;}
	public void setIsBurn(boolean isBurn2){this.isBurn = isBurn2;}
	
	public String getTipe(){return tipe;}
	public void setTipe(String tipe){this.tipe = tipe;}
	
	public void attack(Player chara){
		if(chara.getTipe().equals("Magician")){
			chara.setHp(chara.getHp() - 20);
		}else{
			chara.setHp(chara.getHp() - 10);
		}
	}
	public boolean canEat(Player charaEaten){
		if(getTipe().equals("Human") || getTipe().equals("Magician")){
			if(getIsDead() == false && charaEaten.getIsBurn() == true && charaEaten.getTipe().equals("Monster")){
				return true;
			}
		}else{
			if(getIsDead() == false && charaEaten.getIsDead() == true){
				return true;
			}
		} return false;
	}
	
	public void memangsa(String mangsa){
		diet.add(mangsa);
	}
	//attack mengurangi HP musuh sebanyak 10
	//eat mendapat tambahan 15
	//player matang jika mati kena burn
	//player mati ketika hp 0, nilai minimal dari hp adalah 0. 
	//Sehingga ketika diserang dan nilai hp menjadi < 0, hp yang tersimpan tetap 0. 
	//Ketika assigment nyawa Player dapat bernilai kurang dari sama dengan 0
	
}