package customer;

import theater.Theater;
import ticket.Ticket;
import movie.Movie;


public class Customer{
	private String name;
	private String jenisKelamin;
	private int age;
	
	public Customer(String name, String jenisKelamin, int age){
		this.name = name;
		this.jenisKelamin = jenisKelamin;
		this.age = age;
	}
	
	public String getName(){return name;}
	public void setName(String name){this.name = name; }
	public void setJenisKelamin(String jenisKelamin){this.jenisKelamin = jenisKelamin;}
	public String getJenisKelamin(){return jenisKelamin;}
	public int getAge(){return age;}
	public void setAge(int age){this.age = age;}
	
	
	public Ticket orderTicket(Theater bioskop, String movieName, String hari, String jenis){
		boolean dimensi3D;
		if(jenis.equals("3 Dimensi")){//untuk memeriksa apakah film dalam jenis 3D atau tidak
			dimensi3D = true;
		}else{
			dimensi3D = false;
		}
		
		boolean checked = false;
		Ticket ticket = null;
		int g = 0;
		
		for(g = 0; g < bioskop.getTickets().size(); g++){
		ticket = bioskop.getTickets().get(g);
			
		if(ticket.getmovieTiket().getmovieName().equals(movieName) && ticket.getHari().equals(hari) && ticket.getJenis2().equals(jenis)){
		checked = true;
		break;
		}
	}
			if(checked){
				if(ticket.getmovieTiket().getRating().equals("Remaja") && getAge() < 13 || ticket.getmovieTiket().getRating().equals("Dewasa") && getAge() < 17){
					System.out.format("%s masih belum cukup umur untuk menonton %s dengan rating %s \n", getName(), movieName, ticket.getmovieTiket().getRating());
				}else{
					System.out.format("%s telah membeli tiket %s jenis %s di %s pada hari %s seharga Rp. %d \n", getName(), movieName, jenis, bioskop.getTheaterName(), hari, ticket.getHarga());
					bioskop.setSaldo(bioskop.getSaldo() + ticket.getHarga());
					return ticket;
				}
			}else{
				System.out.format("Tiket untuk film %s jenis %s dengan jadwal %s tidak tersedia di %s \n", movieName, jenis, hari, bioskop.getTheaterName());
			}
		return null;
	}


	public void findMovie(Theater bioskop, String movieName){ //nyari apakah film yg mau diputer ada di bioskop ini apa ngga
		boolean checked = false;
		int i = 0;
		Movie[] film = bioskop.getmovieNames();
		
		for(i = 0; i<film.length; i++){
			if(film[i].getmovieName().equals(movieName)){
				checked = true;
				break;
			}else{
				checked = false;
			}
		}	
		if(checked){
			System.out.println(film[i].MovieInfo());
		}else{
			System.out.format("Film %s yang dicari %s tidak ada di bioskop %s \n", movieName, this.name, bioskop.getTheaterName());
		}	
	}
}