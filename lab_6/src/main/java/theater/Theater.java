package theater;

import movie.Movie;
import ticket.Ticket;
import java.util.ArrayList;
import java.util.Arrays;


public class Theater{
	private String theaterName;
	private int saldo;
	private ArrayList<Ticket> Tickets;
	private Movie[] movieNames;
	
	public Theater(String theaterName, int saldo, ArrayList<Ticket> Tickets, Movie[] movieNames){
		this.theaterName = theaterName;
		this.saldo = saldo;
		this.Tickets = Tickets;
		this.movieNames = movieNames;
	}
	public void setTheaterName(String theaterName){
		this.theaterName = theaterName;
	}
	public String getTheaterName(){
		return theaterName;
	}
	public void setSaldo(int saldo){
		this.saldo = saldo;
	}
	public int getSaldo(){
		return saldo;
	}
	public void setTickets(ArrayList<Ticket> Tickets){
		this.Tickets = Tickets;
	}
	public ArrayList<Ticket> getTickets(){
		return Tickets;
	}
	public Movie[] getmovieNames(){
		return movieNames;
	}
	public void setmovieNames(Movie[] movieNames){
		this.movieNames = movieNames;
	}
	
	
	public void printInfo(){
		String judulfilm = "";
		for(int k = 0; k<getmovieNames().length; k++){
			if(k == getmovieNames().length-1){
				judulfilm += getmovieNames()[k].getmovieName();
			}else{
				judulfilm += getmovieNames()[k].getmovieName()+", ";
			}
		}
		System.out.println("------------------------------------------------------------------"+
		"\nBioskop\t\t\t: " + this.getTheaterName() + 
		"\nSaldo\t\t\t: " + this.getSaldo() + 
		"\nJumlah tiket tersedia\t: " + this.getTickets().size() + 
		"\nDaftar Film tersedia\t: " + judulfilm +
		"\n------------------------------------------------------------------");
	}
	

	
	public static void printTotalRevenueEarned(Theater[] theaters){ //ngeprint penghasilan semua bioskop
		int pendapatan = 0;
		for(int i = 0; i < theaters.length; i++){
			pendapatan += theaters[i].getSaldo();
			System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + pendapatan + "\n");
		}
		System.out.println("------------------------------------------------------------------");
		for(int j = 0; j < theaters.length; j++){
			System.out.println("\nBioskop\t\t: " + theaters[j].getTheaterName()
		+ "\nSaldo Kas\t: " + theaters[j].getSaldo() + "\n");
		}System.out.println("------------------------------------------------------------------");
}
}