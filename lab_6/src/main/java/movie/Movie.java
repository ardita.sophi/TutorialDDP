package movie;

import java.util.Arrays;

public class Movie{
	private String movieName;
	private String rating;
	private int duration;
	private String genre;
	private String produk;
	
	
	public Movie(String movieName, String rating, int duration, String genre, String produk){
		this.movieName = movieName;
		this.rating = rating;
		this.duration = duration;
		this.genre = genre;
		this.produk = produk;
	}
	
	public String getmovieName(){
		return movieName;
	}
	public void setmovieName(String movieName){
		this.movieName = movieName;
	}
	public String getRating(){
		return rating;
	}
	public void setRating(String rating){
		this.rating = rating;
	}
	public int getDuration(){
		return duration;
	}
	public void setDuration(int duration){
		this.duration = duration;
	}
	public String getGenre(){
		return genre;
	}
	public void setGenre(){
		this.genre = genre;
	}
	public String getProduk(){
		return produk;
	}
	public void setProduk(String produk){
		this.produk = produk;
	}
	
	public String MovieInfo(){
		return("\n------------------------------------------------------------------" +
		"\nJudul\t: " + this.getmovieName() + 
		"\nGenre\t: " + this.getGenre() + 
		"\nDurasi\t: " + this.getDuration() + " menit" +
		"\nRating\t: " + this.getRating() + 
		"\nJenis\t: " + "Film " +this.getProduk() + 
		"\n------------------------------------------------------------------");
	}
}
