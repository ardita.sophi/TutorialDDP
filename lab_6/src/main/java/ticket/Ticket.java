package ticket;

import movie.Movie;
import java.util.Arrays;

public class Ticket{
	private Movie movieTiket;
	private String hari, jenis2;
	private boolean jenis; //biasa(2D) atau 3D
	private int harga = 60000;
	
	public Ticket(Movie movieTiket, String hari, boolean jenis){
		this.movieTiket = movieTiket;
		this.hari = hari;
		this.jenis = jenis;
		if(this.hari.equals("Sabtu") || this.hari.equals("Minggu")){ //kalo weekend, harga tambah 40 ribu
			this.harga += 40000;
		}if(this.jenis == true){ //kalo 3D, harga lebih mahal 20%
			this.harga += (harga * 0.2);
		}
		this.jenis2 = (jenis) ? "3 Dimensi" : "Biasa";
	}
	
	public String getJenis2(){return this.jenis2;}
	
	public String getHari (){
		return hari;
	}
	public void setHari(String hari){
		this.hari = hari;
	}
	public boolean getJenis(){
		return jenis;
	}
	public void setJenis(boolean jenis){
		this.jenis = jenis;
	}
	public int getHarga(){
		return harga;
	}
	public void setHarga(int harga){
		this.harga = harga;
	}
	public Movie getmovieTiket(){
		return movieTiket;
	}
	public void setmovieTiket(Movie movieTiket){
		this.movieTiket = movieTiket;
	}

	
	public void infoTicket(){//buat ngecek 3d/2d
		if(this.getJenis() == true){
			System.out.println("------------------------------------------------------------------\n Judul\t\t: " + this.movieTiket + 
			"\nJadwal Tayang\t: " + this.hari + "\nJenis\t\t: 3 Dimensi");
		}else{
			System.out.println("------------------------------------------------------------------\n Judul\t\t: " + this.movieTiket + 
			"\nJadwal Tayang\t: " + this.hari + "\nJenis\t\t: Biasa");
		}
	}
}