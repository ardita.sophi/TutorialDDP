package lab9.event;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.math.BigInteger;


/**
* A class representing an event and its properties
*/
public class Event
{
    /** Name of event */
    private String name;
    
    // TODO: Make instance variables for representing beginning and end time of event
    private LocalDateTime startTime;
	private LocalDateTime endTime;
    // TODO: Make instance variable for cost per hour
    private BigInteger cost;
    // TODO: Create constructor for Event class
    public Event(String name, LocalDateTime startTime, LocalDateTime endTime, BigInteger cost){
		this.name = name;
		this.startTime = startTime;
		this.endTime = endTime;
		this.cost = cost;
	}
    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public String getName()
    {
        return this.name;
    }
    
    // TODO: Implement toString()
	public String toString(){ 
		DateTimeFormatter formatting = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		 // Membuat tipe objek String dari format tanggal dan waktu objek Local Date Time
		String dateTimeStart = startTime.format(formatting);
		String dateTimeEnd = endTime.format(formatting);
		
		String[] begin = dateTimeStart.split(" ");
		String[] ended = dateTimeEnd.split(" ");
		return (name + "\n" 
				+ "Waktu mulai: " + begin[0] + ", " + begin[1] + "\n" + "Waktu selesai: " + ended[0] + ", " + ended[1] + "\n" + "Biaya kehadiran: " + cost);
	}
	
	public BigInteger getCost(){
		return cost;
	}
	
	public LocalDateTime getStartTime(){
		return startTime;
	}
	public LocalDateTime getEndTime(){
		return endTime;
	}
    
    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.
	
	public boolean overlapsWith(Event other){
		if(this.startTime.isAfter(other.endTime)||this.endTime.isBefore(other.startTime)){
			return false;
		}
		return true;
	}
}
