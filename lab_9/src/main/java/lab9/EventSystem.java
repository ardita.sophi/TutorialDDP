package lab9;

import lab9.user.User;
import lab9.event.Event;
import java.util.ArrayList;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
* Class representing event managing system
*/
public class EventSystem
{
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem()
    {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }
	
	public Event findEvent(String name){
		for(int i = 0; i<events.size(); i++){
			if(events.get(i).getName().equals(name)){
				return events.get(i);
			}
		}return null;
	}
	
	public User findUser(String username){
		for(int i = 0; i<users.size(); i++){
			if(users.get(i).getName().equals(username)){
				return users.get(i);
			}
		}return null;
	}
    
	
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr)
    {
        // TODO: Implement!
		String begin = startTimeStr.replace("_", " ");
		String finish = endTimeStr.replace("_", " ");
		DateTimeFormatter formatting = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime formatMulai = LocalDateTime.parse(begin, formatting);
		LocalDateTime formatAkhir = LocalDateTime.parse(finish, formatting);
		
		Event anEvent = findEvent(name);
		BigInteger cost = new BigInteger(costPerHourStr);
		if(anEvent != null){
			return "Event " +name+ " sudah ada!";
		}else{
			if(formatAkhir.isBefore(formatMulai)){
				return "Waktu yang dimaksud tidak valid";
			}else{
				events.add(new Event(name, formatMulai, formatAkhir, cost));
				return "Event " + name + " berhasil ditambahkan!";
			}
		}
	}
    
    public String addUser(String name)
    {
        // TODO: Implement:
		User theUser = findUser(name);
		if(theUser != null){
			return "User " + name + " sudah ada!";
		}else{
			users.add(new User(name));
			return "User " + name + " berhasil ditambahkan!";
		}
    }
	
    /**
	Method untuk mencetak informasi User
	*/
	
	public User getUser(String name){
		return findUser(name);
	}
	/**
	Method untuk mencetak informasi suatu Event
	*/
	public String getEvent(String event){
		Event event1 = findEvent(event);
		if(event == null){
			return "Tidak ada acara bernama " + event;
		}else{
			return event.toString();
		}
	}
	
    public String registerToEvent(String userName, String eventName)
    {
        // TODO: Implement
		User userT = findUser(userName);
		Event EventT = findEvent(eventName);
		if(userT == null && EventT == null){
			return "Tidak ada user dengan nama " + userName + " dan acara bernama " + eventName;
		}else if(userT == null){
			return "Tidak ada user dengan nama " + userName;
		}else if(EventT == null){
			return "Tidak ada acara bernama "+ eventName;
		}else{
			if(!userT.addEvent(EventT)){
				return userName + " sibuk sehingga tidak dapat menghadiri " + eventName +"!";
			}else{
				return userName + " berencana menghadiri " + eventName + "!";
			}
		}
    }
}
